import React, { useEffect } from 'react';
import {connect} from "react-redux";

import Header from "../Header";
import Chart from "../Chart";
import UsersList from "../UsersList";
import {parseData} from "../../actions/parseData";

const App = ({parseData}) => {

    useEffect(() => {
        const url = 'wss://crash.heja.games/ws';
        const connection = new WebSocket(url);

        connection.onopen = () => {};

        connection.onmessage = function(event) {
            parseData(event);
        };

        connection.onerror = error => {
            console.log(`WebSocket error: onopen ${error}`)
        }
    });

    return (
        <div className='main'>
            <Header/>
            <div className='container'>
                <div className="row my-4">
                    <Chart/>
                    <UsersList/>
                </div>
            </div>
        </div>
    );
};

export default connect(
    null,
    dispatch => ({
        parseData: data => {dispatch(parseData(data))}
    })
)(App);
