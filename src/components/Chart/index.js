import React, {Component} from 'react';
import {connect} from "react-redux";

import * as PIXI from 'pixi.js'
import * as PropTypes from "prop-types";

class Chart extends Component {

    constructor(props) {
        super(props);

        this.x = 0;
        this.pixi_cnt = null;
        this.w = 350;
        this.h = 350;

        this.lastGameX = 0;
        this.lastGameY = 0;

        this.app = new PIXI.Application({width: this.w, height: 360, backgroundColor:'0x607d8b'});

        this.message = new PIXI.Text('0x');
        this.message.x = 95;
        this.message.y = 110;

        this.line = new PIXI.Graphics();
    }

    updatePixiCnt = (element,coef,is_game_going) => {
        this.pixi_cnt = element;
        this.updateMessage(coef, is_game_going);
        this.updateX(is_game_going);
        this.updateLine(coef,is_game_going);

        if(this.pixi_cnt && this.pixi_cnt.children.length<=0) {
            this.app.stage.addChild(this.message);

            this.app.stage.addChild(this.line);
            this.app.stage.setChildIndex(this.line, 0);

            this.pixi_cnt.appendChild(this.app.view);
        }
    };

    updateLine(coef,is_game_going){
        coef *= coef;
        let y, y2;
        if(coef <= this.h){
            y = this.h - coef;
            y2 = this.h - coef/4;
        }else{
            y = 0;
            y2 = 3*this.h/4;
        }


        let x = (is_game_going)?this.x:this.lastGameX;
        let color = (is_game_going)?0x0ee07b:0xa1a1a1;

        this.line.clear();
        this.line.lineStyle(6, color, 1);
        this.line.moveTo(0,this.h);
        this.line.bezierCurveTo(0,this.h,x/2,y2,x,y);
    }

    updateMessage(coef, is_game_going){
        this.message.text = (!is_game_going)?`Crashed @ ${coef}x`:`${coef}x`;
        this.message.style.fill = (!is_game_going)?0xff0000:0xffffff;
    }

    updateX(is_game_going){
        if(!is_game_going) {
            this.lastGameX = this.x;
            return this.x=0;
        }
        this.lastGameX = 0;
        if(this.x < this.w) this.x++;
    }

    render() {
        let {coef,is_game_going} = this.props;
        return (
            <div className='col-6' ref={el => this.updatePixiCnt(el,coef,is_game_going)} />
        );
    }
}

Chart.propTypes = {coef: PropTypes.any};

export default connect(
    state => ({
        coef: state.game.coef,
        is_game_going: state.game.is_game_going,
    })
)(Chart);