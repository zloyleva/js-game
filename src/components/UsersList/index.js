import React from 'react';
import {connect} from "react-redux";

import UsersListHeader from "./UsersListHeader";
import UserRow from "../UserRow";

import './style.css';

const UsersList = ({users: {current_game, last_game}}) => {
    let users = [];
    let game_status_class = '';
    if(last_game.length){
        users = last_game;
        game_status_class = 'finish'
    }else {
        users = current_game;
        game_status_class = ''
    }
    return (
        <div className={`${game_status_class} col-6`}>
            <table className="table table-striped">
                <UsersListHeader />
                <tbody>
                    {
                        users.map(el => (
                          <UserRow user={el} key={el.user.id}/>
                        ))
                    }
                </tbody>
            </table>
        </div>
    );
}

export default connect(
    state => ({
        users: state.users
    })
)(UsersList);