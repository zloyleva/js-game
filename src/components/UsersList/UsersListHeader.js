import React from 'react';

const UsersListHeader = () => {
    return (
      <thead>
          <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">Bet amount</th>
              <th scope="col">coef</th>
              <th scope="col">profit</th>
          </tr>
      </thead>
    );
}

export default UsersListHeader;