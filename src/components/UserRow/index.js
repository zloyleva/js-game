import React from 'react';
import './user_item.css'

const UserRow = ({user}) => {
    console.log(user.user);

    const player_leaved = user.coef?'player_leaved':'';

    return (
      <tr className={`user_item ${player_leaved}`}>
        <td scope="row">{user.user.id}</td>
        <td>{user.user.username}</td>
        <td>{user.bet_amount}</td>
        <td>{user.coef?user.coef:'--'}</td>
        <td>{user.profit?user.profit:'--'}</td>
      </tr>
    );
}

export default UserRow;