import {GAME_CRASH, GAME_TICK} from "../actions/game";

const initState = {
    coef: 0,
    current_millis: 0,
    next_round_millis: 0,
    is_game_going: true
};

export const game = (state = initState, {type, payload}) => {

    switch (type) {
        case GAME_TICK:
            return {
                ...state,
                is_game_going:true,
                ...payload
            };
        case GAME_CRASH:
            return {
                ...state,
                ...payload,
                is_game_going:false
            };
        default:
            return state
    }
};