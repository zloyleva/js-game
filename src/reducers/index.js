import { users } from "./users";
import { game } from "./game";
import { combineReducers } from "redux";


export default combineReducers({
    users,
    game,
})