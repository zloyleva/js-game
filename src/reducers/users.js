import {PLAYER_JOINED, PLAYER_LEAVED} from "../actions/player";
import {GAME_CRASH, GAME_TICK} from "../actions/game";

const initState = {
    current_game: [],
    last_game: [],
};

export const users = (state = initState, {type, payload}) => {

    switch (type) {
        case PLAYER_JOINED:
            state.current_game.push(payload);
            return {
                ...state,
                last_game: []
            };
        case PLAYER_LEAVED:
            return {
                ...state,
                current_game: state.current_game
                    .map((el) => el.user.id === payload.user.id?{...el, coef:payload.coef,profit:payload.profit}:el)
            };
        case GAME_TICK:
            return {
                ...state,
                last_game: []
            };
        case GAME_CRASH:
            return {
                current_game: [],
                last_game: state.current_game
                    .map(el => el.coef?el:{...el, profit:-1*el.bet_amount})
            };
        default:
            return state
    }
};