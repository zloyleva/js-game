export const PLAYER_JOINED = 'PLAYER_JOINED';
export const PLAYER_LEAVED = 'PLAYER_LEAVED';
export const PLAYER_CLEARED = 'PLAYER_CLEARED';

export const player_joined = ({payload}) => ({type: PLAYER_JOINED, payload: payload});
export const player_leaved = ({payload}) => ({type: PLAYER_LEAVED, payload: payload});
export const player_cleared = () => ({type: PLAYER_CLEARED});