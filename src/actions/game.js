export const GAME_TICK = 'GAME_TICK';
export const GAME_CRASH = 'GAME_CRASH';

export const game_tick = ({payload}) => ({type: GAME_TICK, payload: payload});
export const game_crash = ({payload}) => ({type: GAME_CRASH, payload: payload});