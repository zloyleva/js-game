import {player_joined, player_leaved} from "./player";
import {game_crash, game_tick} from "./game";

export const parseData = ({data}) => dispatch => {

    data = JSON.parse(data);
    console.log(data);

    switch (data.type) {
        case 'player_joined':
            dispatch(player_joined(data));
            break;
        case 'player_leaved':
            dispatch(player_leaved(data));
            break;
        case 'game_tick':
            dispatch(game_tick(data));
            break;
        case 'game_crash':
            dispatch(game_crash(data));
            break;
    }
}